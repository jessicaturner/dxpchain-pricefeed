#!/usr/bin/env python

from setuptools import setup, find_packages
import sys

__VERSION__ = '0.0.10'

assert sys.version_info[0] == 3, "DxpChain-PriceFeed requires Python > 3"

setup(
    name='dxpchain-pricefeed',
    version=__VERSION__,
    description='Command line tool to assist with price feed generation',
    long_description=open('README.md').read(),
    download_url='https://github.com/xeroc/dxpchain-pricefeed/tarball/' + __VERSION__,
    author='Fabian Schuh',
    author_email='Fabian@chainsquad.com',
    maintainer='Fabian Schuh',
    maintainer_email='Fabian@chainsquad.com',
    url='http://www.github.com/xeroc/dxpchain-pricefeed',
    keywords=['dxpchain', 'price', 'feed', 'cli'],
    packages=find_packages(),
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
    ],
    entry_points={
        'console_scripts': [
            'dxpchain-pricefeed = dxpchain_pricefeed.cli:main'
        ],
    },
    install_requires=[
        "pandas<1.3",
        "requests",
        "prettytable",
        "click",
        "colorama",
        "tqdm",
        "pyyaml",
        "quandl"
    ],
    extras_require = {
        'history_db_postgresql':  ["SQLAlchemy", "py-postgresql"]
    },
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    include_package_data=True,
)
