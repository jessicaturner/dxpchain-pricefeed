# If true, a new price feed needs manual confirmation
confirm: True

# The producer name(s)
producer: $PRODUCER

# Exchange settings (Here, you may need to add API keys)
exchanges:
    ####################################
    # Intermediate assets exchange rates
    ####################################
    btc_and_eth_price:
        klass: Composite
        aggregation_type: 'weighted_mean'
        exchanges:
            coinbase:
                klass: Coinbase
                quotes:
                    - BTC
                    - ETH
                bases:
                    - USD
            kraken:
                klass: Kraken
                quotes:
                    - XXBT
                    - XETH
                bases:
                    - ZUSD
                aliases:
                    XXBT: BTC
                    XETH: ETH
                    ZUSD: USD
            bitstamp:
                klass: Bitstamp
                quotes:
                    - BTC
                    - ETH
                bases:
                    - USD

    usdt_price:
        klass: Composite
        aggregation_type: 'weighted_mean'
        exchanges:
            bittrex:
                klass: Bittrex
                quotes:
                    - USDT
                bases:
                    - USD
            kraken:
                klass: Kraken
                quotes:
                    - USDT
                bases:
                    - ZUSD
                aliases:
                    ZUSD: USD

    ####################################
    # Crypto Exchange Rates
    ####################################
    aex:
        klass: Aex
        quotes:
            - DXP
        bases:
            - USDT
            - CNC
        aliases:
            CNC: CNY
    binance:
        klass: Binance
        quotes:
            - DXP
        bases:
            - BTC
    openledger:
        klass: Graphene
        quotes:
            - DXP
        bases:
            - OPEN.BTC
        aliases:
            OPEN.BTC: BTC
    gdex:
        klass: Graphene
        quotes:
            - DXP
        bases:
            - GDEX.BTC
        aliases:
            GDEX.BTC: BTC
    dxpchain:
        klass: Graphene
        quotes:
            - CNY
        bases:
            - DXP
        aliases:
            CNY: BITCNY
    cointiger:
        klass: CoinTiger
        quotes:
            - DXP
            - BITCNY
        bases:
            - BTC
            - ETH
    gateio:
        klass: GateIo
        enable: False
        quotes:
            - DXP
        bases:
            - BTC
            - USDT
    huobi:
        klass: Huobi
        quotes:
            - DXP
        bases:
            - BTC
            - USDT
            - ETH
    lbank:
        klass: Lbank
        quotes:
            - DXP
        bases:
            - BTC
            - ETH
    poloniex:
        klass: Poloniex
        quotes:
            - DXP
        bases:
            - BTC
    zb:
        klass: Zb
        quotes:
            - DXP
            - BITCNY
        bases:
            - BTC
            - USDT
            - QC
    qc_fees:
        klass: Manual
        feed:
            CNY:
                QC:
                    price: 0.995
                    volume: 1.0        
        
# default settings
default:
    # max age of a feed (2h)
    maxage: 7200

    # minimum percentage that forces a publish
    min_change: 0.2

    # warn if price change goes above this percentage
    warn_change: 10

    # skip publishing a feed if price goes above this percentage
    skip_change: 20

    # skip publishing a feed if producer is not an active blockproducer.
    skip_inactive_blockproducer: True

    # how to derive a single price from several sources
    # Choose from: "median", "mean", or "weighted" (by volume)
    metric: weighted

    # Select sources for this particular asset. Each source
    # has its own fetch() method and collects several markets
    # any market of an exchanges is considered but only the
    # current asset's price is derived
    #
    # Choose from: - "*": all,
    #              - loaded exchanges (see below)
    sources:
        -*

    # Core exchange factor for paying transaction fees in
    # non-DXP assets. Premium of 5%
    core_exchange_factor: 1.2

    # maintenance collateral ratio (percentage)
    maintenance_collateral_ratio: 175.0

    # Maximum short squeeze ratio
    maximum_short_squeeze_ratio: 110.0

    # If set to True, prices are also derived via 3
    # markets instead of just two:
    # E.g.: GOLD:USD -> USD:BTC -> BTC:DXP = GOLD:DXP
    derive_across_3markets: True


# Enabled assets that are derived if no asset is provided via command
# line
assets:
    USD:
        global_settlement_protection: 2
        maximum_short_squeeze_ratio: 101.0
        maintenance_collateral_ratio: 150.0
        price_threshold: 0.0350
    CNY:
        global_settlement_protection: 2
        maximum_short_squeeze_ratio: 101.0
        maintenance_collateral_ratio: 160.0
        price_threshold: 0.2200

# Intermediate assets are useful for 2 and 3 market price derivation
# E.g.: USD:BTC -> BTC:DXP = USD:DXP
#       GOLD:USD -> USD:BTC -> BTC:DXP = GOLD:DXP
intermediate_assets:
    - USD
    - BTC
    - USDT
    - BITCNY
    - ETH
    - QC