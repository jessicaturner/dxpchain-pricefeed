import requests
from . import FeedSource, _request_headers


class GateIO(FeedSource):
    def _fetch(self):
        feed = {}
        url = "https://api.gateio.ws/api/v4/spot/tickers?currency_pair={quote}_{base}"
        for base in self.bases:
            for quote in self.quotes:
                if quote == base:
                    continue
                response = requests.get(
                    url=url.format(
                        base=base.upper(),
                        quote=quote.upper()
                    ),
                    headers=_request_headers, timeout=self.timeout)
                result = response.json()
                if response.status_code != 200 or 'message' in result:
                    print("Unable to fetch data from GateIO: {}".format(result['message']))
                    continue
                ticker = result[0]
                self.add_rate(feed, base, quote, float(ticker["last"]), float(ticker["quote_volume"]))
        return feed
