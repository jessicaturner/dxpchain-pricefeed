import sys
import requests
import time
from . import FeedSource, _request_headers


class OkExOtc(FeedSource):
    def _fetch(self):
        feed = {}
        t = int(time.time())
        for base in self.bases:
            for quote in self.quotes:
                if quote == base:
                    continue
                url = "https://www.okex.com/v3/c2c/otc-ticker/quotedPrice?t={}&baseCurrency={}&quoteCurrency={}&side=buy&amount=&standard=1&paymentMethod=aliPay".format(t, base.upper(), quote.upper())
                response = requests.get(url=url, headers=_request_headers, timeout=self.timeout)
                result = response.json()
                
                self.add_rate(feed, base, quote, 1. / float(result["data"][0]["price"]), 1)
                feed[self.alias(base)]["response"] = result
        return feed
