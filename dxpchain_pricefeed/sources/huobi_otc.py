import requests
from . import FeedSource, _request_headers
import json


class HuobiOtc(FeedSource):
    def _request(self, side):
        extracted_tickers = {}
        url = "https://otc-api.hbg.com/v1/trade/fast/config/list?side={}&tradeMode=fast".format(side)
        headers = { **_request_headers, 'portal': 'web' }
        response = requests.get(url=url, headers=headers, timeout=self.timeout)
        result = response.json()
        if not result['success'] :
            raise Exception(result['message'])
        for ticker in result['data']:
            for base in self.bases:
                if ticker['cryptoAsset']['name'] == base:
                    extracted_tickers[base] = {}
                    for ticker_quote in ticker['quoteAsset']:
                        if ticker_quote['name'] in self.quotes:
                            extracted_tickers[base][ticker_quote['name']] = 1.0 / float(ticker_quote['price'])
        return extracted_tickers

    def _fetch(self):
        feed = {}

        tickers_buy = self._request('buy')
        tickers_sell = self._request('sell')

        for base in self.bases:
            for quote in self.quotes:
                if quote == base:
                    continue
                mid_price = (tickers_buy[base][quote] + tickers_sell[base][quote]) / 2
                self.add_rate(feed, base, quote, mid_price, 1.0)
                
        return feed
