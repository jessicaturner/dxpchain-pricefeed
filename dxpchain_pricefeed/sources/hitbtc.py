import requests
from . import FeedSource, _request_headers


class HitBTC(FeedSource):
    def _fetch(self):
        feed = {}
        url = "https://api.hitbtc.com/api/3/public/ticker/{quote}{base}"
        for base in self.bases:
            for quote in self.quotes:
                if quote == base:
                    continue
                response = requests.get(
                    url=url.format(
                        base=base.upper(),
                        quote=quote.upper()
                    ),
                    headers=_request_headers, timeout=self.timeout)
                result = response.json()
                if response.status_code != 200 or 'error' in result:
                    print("Unable to fetch data from HitBTC: {}".format(result['error']['message']))
                    continue
                self.add_rate(feed, base, quote, float(result["last"]), float(result["volume"]))
        return feed
