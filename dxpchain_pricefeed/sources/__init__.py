from .main import FeedSource, _request_headers, fetch_all

# Exchanges
from .bitcoinaverage import BitcoinAverage
from .bitcoinvenezuela import BitcoinVenezuela
from .bittrex import Bittrex
from .coincap import Coincap
from .coinmarketcap import Coinmarketcap, CoinmarketcapPro
from .currencylayer import CurrencyLayer
from .fixer import Fixer
from .graphene import Graphene
from .dxpchain_orderbook import DxpchainOrderbook
from .huobi import Huobi
from .indodax import IndoDax
from .okcoin import Okcoin
from .openexchangerate import OpenExchangeRates
from .poloniex import Poloniex, PoloniexVWAP
from .quandl import Quandl
from .bitstamp import Bitstamp
from .aex import Aex
from .zb import Zb
from .lbank import Lbank
from .alphavantage import AlphaVantage
from .binance import Binance
from .iex import Iex
from .worldcoinindex import WorldCoinIndex
from .coindesk import Coindesk
from .dxpchainfeed import DxpchainFeed
from .manual import Manual
from .coinegg import CoinEgg
from .composite import Composite
from .cointiger import CoinTiger
from .magicwallet import MagicWallet
from .hertz import Hertz
from .norm import Norm
from .hero import Hero
from .kraken import Kraken
from .coinbase import Coinbase
from .coingecko import CoinGecko
from .biki import Biki
from .huobi_otc import HuobiOtc
from .gateio import GateIO
from .hitbtc import HitBTC
from .okex_otc import OkExOtc

