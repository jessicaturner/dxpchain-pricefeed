from dxpchain_pricefeed.sources.cointiger import CoinTiger

def test_cointiger_fetch(checkers):
    source = CoinTiger(quotes=['DXP', 'BITCNY', 'FAKE'], bases=['BTC', 'ETH']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['DXP:BTC', 'DXP:ETH', 'BITCNY:BTC', 'BITCNY:ETH'])