from dxpchain_pricefeed.sources.gateio import GateIO

def test_gateio_fetch(checkers):
    source = GateIO(quotes=['DXP', 'ETH'], bases=['BTC']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['DXP:BTC', 'ETH:BTC'])
