from dxpchain_pricefeed.sources.huobi import Huobi

def test_huobi_fetch(checkers):
    source = Huobi(quotes=['DXP', 'ETH'], bases=['BTC', 'USDT']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['DXP:BTC', 'ETH:BTC', 'DXP:USDT', 'ETH:USDT'])


