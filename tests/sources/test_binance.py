from dxpchain_pricefeed.sources.binance import Binance

def test_binance_fetch(checkers):
    source = Binance(quotes=['DXP', 'ETH'], bases=['BTC', 'USDT'], aliases={'USDT':'USD'}) 
    feed = source.fetch()
    checkers.check_feed(feed, ['DXP:BTC', 'ETH:BTC', 'ETH:USD'])


