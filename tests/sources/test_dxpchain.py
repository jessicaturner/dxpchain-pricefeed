from dxpchain_pricefeed.sources.dxpchainfeed import DxpchainFeed

def test_dxpchainfeed_fetch(checkers):
    source = DxpchainFeed(assets=['USD', 'CNY']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['DXP:USD', 'DXP:CNY'], noVolume=True)


