from dxpchain_pricefeed.sources.coingecko import CoinGecko

def test_coingecko_fetch(checkers):
    source = CoinGecko(quotes=['dxpchain', 'bitcoin'], bases=['EUR', 'USD'], aliases={'dxpchain': 'DXP', 'bitcoin': 'BTC'}) 
    feed = source.fetch()
    checkers.check_feed(feed, ['DXP:EUR', 'DXP:USD', 'BTC:USD', 'BTC:EUR'])


