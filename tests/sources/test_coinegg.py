from dxpchain_pricefeed.sources.coinegg import CoinEgg

def test_coinegg_fetch(checkers):
    source = CoinEgg(quotes=['DXP', 'ETH'], bases=['BTC', 'USDT']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['DXP:BTC', 'ETH:BTC', 'ETH:USDT'])


