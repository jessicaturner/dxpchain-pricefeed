from dxpchain_pricefeed.sources.indodax import IndoDax

def test_indodax_fetch(checkers):
    source = IndoDax(quotes=['BTC', 'DXP'], bases=['IDR', 'USD']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['BTC:IDR', 'DXP:IDR'])


