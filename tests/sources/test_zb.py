from dxpchain_pricefeed.sources.zb import Zb

def test_zb_fetch(checkers):
    source = Zb(quotes=['DXP', 'ETH'], bases=['BTC']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['DXP:BTC', 'ETH:BTC'])

def test_zb_fetch_with_alias(checkers):
    source = Zb(quotes=['DXP'], bases=['BTC', 'USDT'], aliases={'USDT': 'USD'}) 
    feed = source.fetch()
    checkers.check_feed(feed, ['DXP:BTC', 'DXP:USD'])
