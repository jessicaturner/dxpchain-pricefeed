from dxpchain_pricefeed.sources.graphene import Graphene

def test_graphene_fetch(checkers):
    source = Graphene(quotes=['USD', 'CNY'], bases=['DXP']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['USD:DXP', 'CNY:DXP'])


