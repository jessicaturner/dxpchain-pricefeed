from dxpchain_pricefeed.sources.dxpchain_orderbook import DxpchainOrderbook

def test_dxpchainorderbook_fetch(checkers):
    source = DxpchainOrderbook(assets=['USD'], quotes=['USD'], bases=['DXP'], mode='center_price', quote_volume=100)
    feed = source.fetch()
    checkers.check_feed(feed, ['USD:DXP'])


