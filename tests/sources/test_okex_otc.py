from dxpchain_pricefeed.sources.okex_otc import OkExOtc

def test_okex_otc_fetch(checkers):
    source = OkExOtc(quotes=['CNY'], bases=['USDT', 'BTC']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['CNY:USDT', 'CNY:BTC'])


