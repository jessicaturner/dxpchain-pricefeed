from dxpchain_pricefeed.sources.huobi_otc import HuobiOtc

def test_huobi_otc_fetch(checkers):
    source = HuobiOtc(quotes=['CNY'], bases=['USDT']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['CNY:USDT'])


