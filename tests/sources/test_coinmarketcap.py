import os
from dxpchain_pricefeed.sources.coinmarketcap import Coinmarketcap, CoinmarketcapPro

def test_coinmarketcap_fetch(checkers):
    source = Coinmarketcap(quotes=['BTC', 'DXP'], bases=['BTC', 'USD']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['BTC:USD', 'DXP:USD', 'DXP:BTC'])


def test_coinmarketcap_altcap_fetch(checkers):
    source = Coinmarketcap(quotes=['ALTCAP', 'ALTCAP.X'], bases=['BTC']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['ALTCAP:BTC', 'ALTCAP.X:BTC'])

def test_coinmarketcap_full_fetch(checkers):
    source = Coinmarketcap(quotes=['ALTCAP', 'DXP'], bases=['BTC', 'USD']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['ALTCAP:BTC', 'DXP:BTC', 'DXP:USD'])

def test_coinmarketcappro_fetch(checkers):
    source = CoinmarketcapPro(quotes=['BTC', 'DXP'], bases=['BTC', 'USD'], api_key=os.environ['COINMARKETCAP_APIKEY']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['BTC:USD', 'DXP:USD', 'DXP:BTC'])
