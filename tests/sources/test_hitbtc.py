from dxpchain_pricefeed.sources.hitbtc import HitBTC

def test_hitbtc_fetch(checkers):
    source = HitBTC(quotes=['DXP', 'ETH'], bases=['BTC']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['DXP:BTC', 'ETH:BTC'])
