from dxpchain_pricefeed.sources.biki import Biki

def test_biki_fetch(checkers):
    source = Biki(quotes=['DXP', 'BTC'], bases=['USDT'], aliases={'USDT':'USD'}) 
    feed = source.fetch()
    checkers.check_feed(feed, ['DXP:USD', 'BTC:USD'])


