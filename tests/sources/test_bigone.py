from dxpchain_pricefeed.sources.bigone import BigONE

def test_bigone_fetch(checkers):
    source = BigONE(quotes=['BTC', 'DXP'], bases=['USDT', 'BTC']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['DXP:BTC', 'BTC:USDT'])


