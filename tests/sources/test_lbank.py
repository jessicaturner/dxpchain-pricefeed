from dxpchain_pricefeed.sources.lbank import Lbank

def test_lbank_fetch(checkers):
    source = Lbank(quotes=['DXP', 'ETH'], bases=['BTC']) 
    feed = source.fetch()
    checkers.check_feed(feed, ['DXP:BTC', 'ETH:BTC'])
