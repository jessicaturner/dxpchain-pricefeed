from dxpchain_pricefeed.pricefeed import Feed

config = 'examples/norm.yaml'

def test_norm_computation(conf, checkers):
    feed = Feed(conf)
    feed.fetch()
    feed.derive({'URTHR', 'VERTHANDI', 'SKULD'})
    prices = feed.get_prices()
    checkers.check_price(prices, 'URTHR', 'DXP', 0.1)
    checkers.check_price(prices, 'VERTHANDI', 'DXP', 0.1)
    checkers.check_price(prices, 'SKULD', 'DXP', 0.1)
