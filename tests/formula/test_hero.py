from dxpchain_pricefeed.pricefeed import Feed

config = 'examples/hero.yaml'

def test_hero_computation(conf, checkers):
    feed = Feed(conf)
    feed.fetch()
    feed.derive({'HERO'})
    prices = feed.get_prices()
    checkers.check_price(prices, 'HERO', 'DXP', 0.1)
